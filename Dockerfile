FROM drupal:9

RUN usermod -u 1000 www-data && groupmod -g 1000 www-data
RUN chown -R 1000:1000 /opt/drupal
USER 1000
