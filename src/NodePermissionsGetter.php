<?php

namespace Drupal\hide_non_editable_content;

/**
 * Class to get node permission strings.
 */
class NodePermissionsGetter {

  public const BYPASS_NODE_ACCESS_PERMISSION = 'bypass node access';

  public const ADMINISTER_NODES_PERMISSION = 'administer nodes';

  /**
   * Get the permission string to edit an owned node of a specific bundle.
   *
   * @see \Drupal\node\NodePermissions::buildPermissions
   */
  public static function getEditOwnPermission(string $bundle): string {
    return "edit own $bundle content";
  }

  /**
   * Get the permission string to edit any node of a specific bundle.
   *
   * @see \Drupal\node\NodePermissions::buildPermissions
   */
  public static function getEditAnyPermission(string $bundle): string {
    return "edit any $bundle content";
  }

  /**
   * Get the permission string to delete an owned node of a specific bundle.
   *
   * @see \Drupal\node\NodePermissions::buildPermissions
   */
  public static function getDeleteOwnPermission(string $bundle): string {
    return "delete own $bundle content";
  }

  /**
   * Get the permission string to delete any node of a specific bundle.
   *
   * @see \Drupal\node\NodePermissions::buildPermissions
   */
  public static function getDeleteAnyPermission(string $bundle): string {
    return "delete any $bundle content";
  }

}
