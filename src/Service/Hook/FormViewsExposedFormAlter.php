<?php

namespace Drupal\hide_non_editable_content\Service\Hook;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\hide_non_editable_content\NodePermissionsGetter;
use Drupal\views\ViewExecutable;

/**
 * Class to alter the exposed form of views.
 */
class FormViewsExposedFormAlter {

  /**
   * Constructor.
   */
  public function __construct(
    private readonly EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    private readonly AccountProxyInterface $currentUser,
  ) {
  }

  /**
   * Exclude non-editable content from the type filter.
   */
  public function alter(
    array &$form,
    FormStateInterface $form_state,
  ): void {
    $view = $form_state->get('view');
    if (
      !$view instanceof ViewExecutable
      || $view->id() !== 'content'
    ) {
      return;
    }
    $node_bundle_infos = $this->entityTypeBundleInfo->getBundleInfo('node');
    foreach ($node_bundle_infos as $bundle => $info) {
      $has_bypass_node_access_permission = $this->currentUser->hasPermission(
        NodePermissionsGetter::BYPASS_NODE_ACCESS_PERMISSION,
      );
      $has_administer_nodes_permission = $this->currentUser->hasPermission(
        NodePermissionsGetter::ADMINISTER_NODES_PERMISSION,
      );
      $has_edit_own_permission = $this->currentUser->hasPermission(
        NodePermissionsGetter::getEditOwnPermission($bundle),
      );
      $has_delete_own_permission = $this->currentUser->hasPermission(
        NodePermissionsGetter::getDeleteOwnPermission($bundle),
      );
      $has_edit_any_permission = $this->currentUser->hasPermission(
        NodePermissionsGetter::getEditAnyPermission($bundle),
      );
      $has_delete_any_permission = $this->currentUser->hasPermission(
        NodePermissionsGetter::getDeleteAnyPermission($bundle),
      );
      if (
        $has_bypass_node_access_permission
        || $has_administer_nodes_permission
        || $has_edit_own_permission
        || $has_delete_own_permission
        || $has_edit_any_permission
        || $has_delete_any_permission
      ) {
        continue;
      }
      unset($form['type']['#options'][$bundle]);
    }
  }

}
