<?php

namespace Drupal\hide_non_editable_content\Service\Hook;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\hide_non_editable_content\NodePermissionsGetter;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Class to alter the views query.
 */
class ViewsQueryAlter {

  /**
   * Constructor.
   */
  public function __construct(
    private readonly EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    private readonly AccountProxyInterface $currentUser,
  ) {
  }

  /**
   * Alter the views query of the view content to exclude non-editable content.
   */
  public function alter(ViewExecutable $view, QueryPluginBase $query): void {
    if ($view->id() !== 'content') {
      return;
    }
    $node_bundle_infos = $this->entityTypeBundleInfo->getBundleInfo('node');
    foreach ($node_bundle_infos as $bundle => $info) {
      $has_bypass_node_access_permission = $this->currentUser->hasPermission(
        NodePermissionsGetter::BYPASS_NODE_ACCESS_PERMISSION,
      );
      $has_administer_nodes_permission = $this->currentUser->hasPermission(
        NodePermissionsGetter::ADMINISTER_NODES_PERMISSION,
      );
      $has_edit_any_permission = $this->currentUser->hasPermission(
        NodePermissionsGetter::getEditAnyPermission($bundle),
      );
      $has_delete_any_permission = $this->currentUser->hasPermission(
        NodePermissionsGetter::getDeleteAnyPermission($bundle),
      );
      if (
        $has_bypass_node_access_permission
        || $has_administer_nodes_permission
        || $has_edit_any_permission
        || $has_delete_any_permission
      ) {
        continue;
      }
      $has_edit_own_permission = $this->currentUser->hasPermission(
        NodePermissionsGetter::getEditOwnPermission($bundle),
      );
      $has_delete_own_permission = $this->currentUser->hasPermission(
        NodePermissionsGetter::getDeleteOwnPermission($bundle),
      );
      $group_id = $query->setWhereGroup();
      if ($has_edit_own_permission || $has_delete_own_permission) {
        $query->where[$group_id]['conditions'][] = [
          'field' => 'node_field_data.uid',
          'value' => $this->currentUser->id(),
          'operator' => '=',
        ];
        continue;
      }
      $query->where[$group_id]['conditions'][] = [
        'field' => 'node_field_data.type',
        'value' => $bundle,
        'operator' => '!=',
      ];
    }
  }

}
